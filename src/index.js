const Koa = require('koa');
const Twitter = require('twitter');
const app = new Koa();
const Router = require('koa-router');

const appport = process.env.PORT || 5000;
const appaddr = '0.0.0.0';

const client = new Twitter({
    consumer_key: process.env.TWITTER_CONSUMER_KEY,
    consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
    access_token_key: process.env.TWITTER_ACCESS_TOKEN_KEY,
    access_token_secret: process.env.TWITTER_ACCESS_TOKEN_SECRET
});


const router = new Router();
async function tagHandler (ctx) {
    let hashTag = ctx.params.hashTag;
    console.log('Received hashTag ', hashTag);
    ctx.type = 'application/json; charset=utf-8';
    let twits = await client.get('search/tweets', {q: hashTag});
    ctx.body = {
        request_overview: {
            status: 'request success',
            hashTag: twits.search_metadata.query,
            twits_count: twits.search_metadata.count
        },
        payload: twits.statuses
    };
    // console.dir(twits, {depth: 1, colors: true});

}
router.get('/getTwits/:hashTag', tagHandler);

const err = async function (ctx, next){
    try {
        await next();
    } catch (err) {
        // will only respond with JSON
        ctx.status = err.statusCode || err.status || 500;
        ctx.body = {
            message: err.message
        };
    }
};

const reqLogger = async function (ctx, next){
    console.log('Get request from: ', ctx.request.header.host, 'path', ctx.request.url);
    await next();
};

app
    .use(err)
    .use(reqLogger)
    .use(router.routes())
    .use(router.allowedMethods());


app.listen(appport);
console.log('Sever started on port :', appport);
